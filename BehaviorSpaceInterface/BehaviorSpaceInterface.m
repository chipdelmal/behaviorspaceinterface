(* Mathematica Package *)
(* Programmed by: Hector Manuel Sanchez Castellanos *)
(* Created by the Wolfram Workbench Sep 2, 2013 *)

BeginPackage["BehaviorSpaceInterface`"]

GetRunHeader::usage = "Gets an array of data in standard format and returns the header section of a run"
GetRunOutput::usage = "Gets an array of data in standard format and returns the output section of a run in the form: {tick1Out, tick2Out, ... , tickNOut}"
GetExperimentHeaders::usage = "Gets an list of arrays of data in standard format and returns their headers in order"
GetExperimentOutputs::usage = "Gets an list of arrays of data in standard format and returns their outputs in order"
IndependentVariableBins::usage = "Gets the headers section of an experiment and returns the different values the independent variable can take"
IndependentVariableValues::usage = "Gets the headers section of an experiment and returns a list of values the independent variable takes in each run"
GetExperimentOutputsByBins::usage = "Gets the experiment headers and outputs lists and returns a list of the runs outputs grouped by independent parameter values"
GetSelectedDependentVariableVertical::usage = "Gets the outputs organized in bins and returns the selected output across runs of the same bin"
GetSelectedDependentVariableHorizontal::usage = "Gets the outputs organized in bins and returns the selected output by run"
GetSelectedDependentVariableOnTick::usage = "Gets the outputs organized in bins and returns the variable value in the selected tick on every bin"
GetRunOutputWithTail::usage = "Gets the outputs (middle section) of a run that contains tail information (--.-- is the separator)"
GetRunTail::usage = "Gets the tail of a run"
GetExperimentOutputsWithTail::usage = "Gets the outputs of an experiment that contains data with tail information"
GetExperimentTails::usage = "Gets the tails of an experiment that contains them"
NetLogoListToMathematicaList::usage = "Converts a NetLogo list into a Mathematica one (replaces  ->, and []->{})"
FixBitLists::usage = "Converts NetLogo bites lists into Mathematica bites lists (depreciated)"
BitGraph::usage = "Undirected bites graph (depreceated)"
BitIntersectionCount::usage = "Returns the number of times two individuals have been bitten by the same mosquito (depreciated)"
ExperimentIntersectionCount::usage = ""
TotalExperimentIntersectionCount::usage = ""
FixBitesListTail::usage = "Converts NetLogo bites lists into Mathematica bites lists"
FixBitesListTails::usage = "Converts NetLogo experiment bites lists into Mathematica bites lists"
GetRunBloodExchanges::usage = "Gets the run's list of blood exchanges (common bites) between individuals"
GetExperimentBloodExchanges::usage = "Gets the experiments's list of blood exchanges (common bites) between individuals"
InfectionMatrixRun::usage = "Obtains the run's list of blood exchanges and returns it in a matrix form"
InfectionMatrixExperiment::usage = "Obtains the experiment's list of blood exchanges and returns it in a matrix form"
MarkovFromFrequencyMatrix::usage = "Converts a frequency matrix into a Markov one"
MarkovGraph::usage = "Creates a Markov graph with colored edges and widths"
SensitivityMeasures::usage = "Returns a list with the variables: {min,mean,median,max,sd}; on each frame of the samplesList"
SeparateAutomaton::usage = "Separates the different values of a CA matrix creating different separate automata"
MergeAutomata::usage = "Merges the values of CA matrix into a full automata one"
EvaluateCellularAutomaton::usage = "Evaluates one epoch of a multilayered CA in which each layer has its own function and after those functions are evluated the system is evaluated by another function as a whole"
FilterVariablesWithRanges::usage = "Filters a list of samples with respect with a restriction pattern that allows ranges of values"
NestListEvaluateCellularAutomaton::usage = "Evaluates a CA iteratively returning every epoch's value"
NestEvaluateCellularAutomaton::usage = "Evaluates a CA iteratively returning only the last epoch's value"
ExtractKernelValuesAndPositions::usage = "Gets the values of the CA matrix filtered by a given kernel and returns them along with their positions"
ReplaceKernelValuesWithPositions::usage = "Replaces the values of a CA matrix after being evaluated so that the new CA contains the processed outputs of the kernel" 
GenerateProcessedCellsMatrix::usage = "Generates a Boolean matrix that will hold the cells state (processed or unprocessed) in a detemrinate epoch"
UpdateProcessedMatrix::usage = "Updates the value of a given cell of the processed matrix so that the cells that have been processed are updated"
RandomUnprocessedPosition::usage = "Select a random position with a TRUE value within a matrix of boolean values"
FixSize::usage = "Adds padding to a CA so that the kernel may be applied even if it goes beyond its scope"
EvaluateRandomCell::usage = "Evaluates a random cell (according to the values of a matrix that holds if a cell has been updated or not) and applies a given function to it"
EvaluateOneEpoch::usage = "Evaluates the whole CA through one epoch"
EvaluateCAThroughEpochs::usage = "Evaluates the CA trhough a finite number of epochs"
XMLDownOneLevel::usage = "Takes the current XML list one level of depth further."
GetExperimentsSetXMLData::usage = "Returns the XML of all the experiments in XML format in the given path (relative to notebook's directory)"
GetExperimentSection::usage = "Returns the sections of the XML experiment file that match the given tag"
GetExperimentHeader::usage = "Returns the experiment's header in raw format"
GetExperimentHeaderWithFixedDate::usage = "Returns the experiment's header with the date in Mathematica's form"
GetExperimentsSetHeader::usage = "Returns the header of a set of experiments with the dates as a range (requires that the section is tagged: 'header')"
GetExperimentParameters::usage = "Returns the parameters that define the behavior of the experiment"
GetExperimentsSetParameters::usage = "Returns the parameters that define the behavior of a set of experiments with the independent variables ordered (requires that the section is tagged: 'parameters')"
GetOutputFromExperimentPositions::usage = "?"
GetPositionsByIndVarBins::usage = "?"
GetExperimentOutputTags::usage = "?"
GetExperimentTicksOutputs::usage = "Extracts the output data in each tick of the experiment"
GetExperimentSetIndexPositionsByIndependentVariableValues::usage = "Returns the indexes that contain the combination of independent variables values"
GetExperimentSetTaggedOutputByIndependentVariableValues::usage = "Returns the tagged output values of the samples that contain the combination of independent variables values (the return list of lists is ordered by samples, not by ticks)"
GetExperimentSetIndexPositionsByIndependentVariableBins::usage = "Returns the indexes (on the imported XML list of samples) from the samples that contain a certain combination of independent variables"
GetTaggedOutputFromExperimentsIndexes::usage = "Returns the tick output of a given XML tag from the samples provided by the list of indexes (on the imported XML list of samples)"
GetTaggedOutputFromTickIndexOnAllIndependentVariablesBins::usage = "Returns all the values of a tagged output variable in a given tick indes (works across independent variables bins)"
(*GetSampleSection::usage = "Legacy version of GetExperimentSection" only used by some functions*)
GetExperimentBitesSection::usage = "Returns the bites section of a single experiment"
GetExperimentsSetBitesSection::usage = "Returns the bites secion of an experiments set"
(*GetAgentsNames::usage = "Individual names of agents"*)
(*GetBitesLists::usage = "List of individuals bitten by each mosquito"*)
(*GetBitesTransitions::usage = "List of transitions of samples (immediate transmision with overlap of one)"*)
(*GetBitesTransitionsCases::usage = "List of all posible transitions between agents"*)
(*GetTransitionFrequencies::usage = "Counts the frequencies of transitions within a sample"*)
(*GetExperimentTransitionFrequencies::usage = "Adds up transition frequencies of the whole study"*)
(*GetExperimentTransitionFrequenciesFromTail::usage = ""*)
GetExperimentInfectionMatrices::usage = "Gets the Frequency and Markov matrices from an experiments"
GetExperimentSetInfectionMatrices::usage = "Gets the Frequency and Markov matrices from an experiments set"
GetExperimentContactsSection::usage = ""
GetExperimentsSetContactsSection::usage = ""
GetExperimentContactsTransitions::usage = ""
GetExperimentContactsTransitionsWithPattern::usage = ""
GetExperimentSetContactsTransitionsWithPattern::usage = ""
GetContactTally::usage = ""
GetExperimentSetContactsTransitionsWithPatternRaw::usage = ""
GetExperimentSetInfectionMatricesRaw::usage = ""
GetInfectionMatrix::usage = ""
GetExperimentTransitionFrequenciesFromTail::usage = ""
LatinHypercubes::usage = ""
FindHouses::usage = ""
GetHousesContactTally::usage = ""
GetExperimentsSetHousesContactsSection::usage = ""
GetExperimentHousesContactsTransitionsWithPattern::usage = ""
GetExperimentSetHousesContactsTransitionsWithPattern::usage = ""
GetExperimentSetHousesContactsTransitionsWithPatternRaw::usage = ""
ConvertTicksToIntegers::usage = ""
FilterBetweenTicks::usage = ""
GetExperimentSetContactsTransitionsWithPatternInIntervals::usage = ""
GraphTransitionsFrequenciesSet::usage = ""

Begin["`Private`"]
(*XML Import Functions*)
XMLDownOneLevel[xmlData_]:=xmlData// Cases[#, XMLElement[_, _, _], {3}] &
GetExperimentsSetXMLData[path_] := Module[{filesNames, allFilesPaths},
  	SetDirectory[NotebookDirectory[] <> path];
  	filesNames = Drop[FileNames[]];
  	allFilesPaths = FileNames["*.xml"];
  	Import /@ allFilesPaths
]
GetExperimentSection[xmlExperimentData_, sectionTitle_] := Cases[xmlExperimentData, XMLElement[sectionTitle, _, _], Infinity]
GetExperimentHeader[xmlExperimentData_] := Module[{temp},
  	temp = GetExperimentSection[xmlExperimentData, "header"];
  	Cases[temp, XMLElement[a_, _, {c_}] -> {a, c}, {3}]
]
GetExperimentHeaderWithFixedDate[xmlExperimentData_] := Module[{header, datePosition},
  	header = GetExperimentHeader[xmlExperimentData];
  	datePosition = (Position[header, {"date", _}] // Flatten)[[1]];
  	ReplacePart[header, {3, 2} -> DateList[header[[datePosition]][[2]]]]
]
GetExperimentsSetHeader[xmlExperimentsDataSet_] := Module[{headers, datePosition, sorted, dateRange},
  	headers = (GetExperimentHeaderWithFixedDate[#] & /@ xmlExperimentsDataSet);
  	datePosition = (Position[ headers[[1]], {"date", _}] // Flatten)[[1]];
  	sorted = headers[[All, datePosition]][[All, 2]] // Sort;
  	dateRange = DateString /@ {sorted // First, sorted // Last};
  	ReplacePart[headers[[1]], datePosition -> {"date", dateRange}]
]
GetExperimentParameters[xmlExperimentData_] := Module[{temp},
  	temp = GetExperimentSection[xmlExperimentData, "parameters"];
  	Cases[temp, XMLElement[a_, {"id" -> b_}, {c_}] -> {a, b, c}, {3}]
]
GetExperimentsSetParameters[xmlExperimentsDataSet_] := Module[{experimentsData, independentPositions, independentTriplets, independentRanges, replacementRules},
  	experimentsData = GetExperimentParameters[#] & /@ xmlExperimentsDataSet;
  	independentPositions = Position[experimentsData[[1]], {"independent", _, _}] // Flatten;
  	independentTriplets = experimentsData[[All, #]] & /@ independentPositions;
  	independentRanges = DeleteDuplicates /@ (ToExpression /@ (#[[All, 3]] & /@ independentTriplets));
  	replacementRules = {#[[1]], 3} -> #[[2]] & /@ ({independentPositions, independentRanges} // Transpose);
  	ReplacePart[experimentsData[[1]], replacementRules]
]
GetTaggedOutputFromExperimentsIndexes[experimentsXMLData_, positionsList_, outputTag_] := Module[{experimentsOutputsSections, output},
  	experimentsOutputsSections = GetExperimentSection[#, "output"] & /@ experimentsXMLData[[positionsList]];
  	output = Cases[#, XMLElement["output", {"id" -> outputTag}, {a_}] -> a] & /@experimentsOutputsSections;
  	Map[ToExpression, output, Infinity]
]
GetExperimentSetIndexPositionsByIndependentVariableBins[experimentsXMLData_] := Module[{parametersList, indParPos, indParVal, tuples, patterns, constant, indParList, tuplesPositions},
  	parametersList = GetExperimentParameters[#] & /@ experimentsXMLData;
  	indParPos = GetExperimentsSetParameters[experimentsXMLData] // Position[#, {"independent", _, _}] & // Flatten;
  	indParVal = (GetExperimentsSetParameters[experimentsXMLData] // Cases[#, {"independent", _, a_} -> a] &);
  	tuples = Tuples[indParVal];
	patterns = Table[
			constant = ConstantArray[{_, _, "dummy"}, parametersList[[1]][[indParPos]] // Length];
    		ReplacePart[constant[[j]], 3 -> tuples[[i]][[j]]]
    	,{i, 1, tuples // Length}, {j, 1, tuples[[1]] // Length}];
  	indParList = #[[indParPos]] & /@ parametersList; 
    tuplesPositions = Position[ToExpression /@ indParList, #] & /@ patterns;
  	{tuplesPositions, tuples} // Transpose
]
GetExperimentOutputTags[experimentsXMLData_] := Module[{output},
  	output = GetExperimentSection[experimentsXMLData[[1]], "output"];
  	{"outputs", Cases[output, XMLElement[_, {"id" -> a_}, _] -> a] // DeleteDuplicates}
]
GetSampleSection[xmlExperimentData_, sectionTitle_] := Cases[{xmlExperimentData}, XMLElement[sectionTitle, _, _], Infinity]
GetExperimentTicksOutputs[xmlSampleData_]:=Module[{fixed,tickXML,test,test2,out},
	tickXML=GetSampleSection[xmlSampleData, "tick"];
	test = Cases[tickXML, XMLElement["tick", {"id" -> a_}, b__] -> {a, b}];
	test2 = (test[[All, 2]][[#]] // Cases[#, XMLElement[_, {"id" -> c_}, {d_}] -> {c, d}] &) & /@ Range[test // Length];
	out = {{ConstantArray["Tick", test // Length], test[[All, 1]]} // Transpose, test2} // Transpose;
	fixed=(Prepend[#[[2]], #[[1]]] & /@ out)//Transpose;
	Prepend[#[[All, 2]] & /@ fixed,fixed[[All,1,1]]]
]
GetExperimentSetIndexPositionsByIndependentVariableValues[xmlSampleData_, independentVariablesValuesList_] := Module[{relationTable, index},
  	relationTable = GetExperimentSetIndexPositionsByIndependentVariableBins[xmlSampleData];
  	index = Position[relationTable[[All, 2]], independentVariablesValuesList] //Flatten;
  	(relationTable[[#, 1]] & /@ index) // Flatten
]
GetExperimentSetTaggedOutputByIndependentVariableValues[xmlSampleData_, independentVariablesValuesList_, outputTag_] := Module[{indexes},
  	indexes = GetExperimentSetIndexPositionsByIndependentVariableValues[xmlSampleData,independentVariablesValuesList];
  	GetTaggedOutputFromExperimentsIndexes[xmlSampleData, indexes, outputTag]
]
GetTaggedOutputFromTickIndexOnAllIndependentVariablesBins[experimentsXMLData_, tickIndex_, outputXMLTag_] := Module[{positions},
  	positions = GetExperimentSetIndexPositionsByIndependentVariableBins[experimentsXMLData][[All, 2]];
  	{((GetExperimentSetTaggedOutputByIndependentVariableValues[experimentsXMLData, #, outputXMLTag] // Transpose) & /@ positions)[[All, tickIndex]],positions}
]

(*Infection Matrices and Bites Lists*)
GetExperimentBitesSection[experimentXMLData_]:=GetExperimentsSetBitesSection[{experimentXMLData}][[1]]
GetExperimentsSetBitesSection[experimentsXMLData_] := Module[{output, bitesListRaw, fixedListsTails},
  	output = GetExperimentSection[#, "output"] & /@ experimentsXMLData;
  	bitesListRaw = Map[Cases[#, XMLElement["output", {"id" -> "biteList"}, a_] -> a] &, output];(*Lists Tails with data in String form*)
  	fixedListsTails = Map[ToString, FixBitesListTails[bitesListRaw], {5}][[All, 1]];(*List Tails with data in Data form*)
  	fixedListsTails
]
GetAgentsNames[transitionCases_] := transitionCases // Flatten // DeleteDuplicates
GetBitesLists[fixedListsTails_] := Map[#[[All, 1]] &, Map[Drop[#, 1] &, #]] & /@ fixedListsTails
GetBitesTransitions[bitesLists_] := ((Partition[#, 2, 1] & /@ #) // Flatten[#, 1] &) & /@bitesLists
GetBitesTransitionsCases[bitesTransitions_] := bitesTransitions // Flatten // DeleteDuplicates // Tuples[#, 2] &
GetTransitionFrequencies[bitesTransitions_, agentsNames_] := Module[{matrices},
  	matrices = Table[Table[Count[j, {agentsNames[[i]], #}] & /@ agentsNames, {i, 1, agentsNames // Length}], {j, bitesTransitions}];
  	{matrices, ConstantArray[agentsNames, matrices // Length]} // Transpose
]
GetExperimentTransitionFrequencies[transitionFrequencies_] := {transitionFrequencies[[All, 1]] // Mean,transitionFrequencies[[1, 2]]}
GetMarkovMatrix[infectionMatrix_] := {MarkovFromFrequencyMatrix[infectionMatrix[[1]]] // N, infectionMatrix[[2]]}
GetExperimentTransitionFrequenciesFromTail[fixedListsTails_] := Module[{bitesLists, bitesTransitions, agentsNames, transitionFrequencies},
  	bitesLists = GetBitesLists[fixedListsTails];
  	bitesTransitions = GetBitesTransitions[bitesLists];
  	agentsNames = GetAgentsNames[bitesTransitions];
  	transitionFrequencies = GetTransitionFrequencies[bitesTransitions, agentsNames];
  	GetExperimentTransitionFrequencies[transitionFrequencies]
]
GetInfectionMatrix[transitionFrequencies_, agentsNames_] := {Cases[transitionFrequencies, {{#, a_}, b_} -> b] & /@ agentsNames, agentsNames}(*Creates infection matrix out of transition frequencies of the experiment*)
GetExperimentSetInfectionMatrices[fixedListsTails_] := Module[{infectionMatrix, markovMatrix},
  	infectionMatrix = GetExperimentTransitionFrequenciesFromTail[fixedListsTails];
  	markovMatrix = GetMarkovMatrix[infectionMatrix];
  	{infectionMatrix, markovMatrix}
]
GetExperimentInfectionMatrices[fixedListTail_]:=GetExperimentSetInfectionMatrices[{fixedListTail}]
GetExperimentSetInfectionMatricesRaw[bitesLists_] := Module[{c},
  c = GetExperimentInfectionMatrices[#] & /@ bitesLists;
  {c[[All, 1, 1]], c[[1, 1, 2]]}
]


GetExperimentContactsSection[experimentXMLData_]:=GetExperimentsSetContactsSection[{experimentXMLData}][[1]]
GetExperimentsSetContactsSection[experimentsXMLData_] := Module[{output, bitesListRaw, fixedListsTails},
  	output = GetExperimentSection[#, "output"] & /@ experimentsXMLData;
  	bitesListRaw = Map[Cases[#, XMLElement["output", {"id" -> "contactList"}, a_] -> a] &, output];(*Lists Tails with data in String form*)
  	fixedListsTails = Map[ToString, FixBitesListTails[bitesListRaw], {5}][[All, 1]];(*List Tails with data in Data form*)
  	fixedListsTails
]
GetExperimentsSetHousesContactsSection[experimentsXMLData_] := Module[{output, bitesListRaw, fixedListsTails},
  	output = GetExperimentSection[#, "output"] & /@ experimentsXMLData;
  	bitesListRaw = Map[Cases[#, XMLElement["output", {"id" -> "contactHousesList"}, a_] -> a] &,output];(*Lists Tails with data in String form*)
  	fixedListsTails = Map[ToString, FixBitesListTails[bitesListRaw], {5}][[All, 1]];(*List Tails with data in Data form*)
  	fixedListsTails
]
GetContactTally[contactsSectionPart_] := Module[{individual, contacts},
  	individual = contactsSectionPart[[All, 1]][[1]];
  	contacts = contactsSectionPart[[All, 1]] // Drop[#, 1] & // Tally;
  	{individual, contacts}
]
GetExperimentContactsTransitions[contactsSection_] := Module[{b, c, labels},
  	b = (GetContactTally[#] & /@ contactsSection);
  	labels = b[[All, 1]];
  	c = Table[
    		If[Count[b[[i, 2]], {#, _}] > 0, 
       		b[[i, 2]][[Position[b[[i, 2]], #][[1]][[1]], 2]], 0] & /@ labels
    	,{i, 1, labels // Length}];
  	{c, labels}
]
GetHousesContactTally[contactsSectionPart_] := Module[{housesNames, individualsNames, tallys},
  	housesNames = Table[i[[All, 1]], {i, contactsSectionPart[[All, 2 ;; All]]}] // Flatten // DeleteDuplicates // Sort;
  	individualsNames = contactsSectionPart[[All, 1]];
  	tallys = Tally /@ contactsSectionPart[[All, 2 ;; All, 1]];
  	{individualsNames, tallys} // Transpose
]
GetExperimentContactsTransitionsWithPattern[contactsSection_, pattern_] := Module[{b, c, labels},
  	b = (GetContactTally[#] & /@ contactsSection) // Sort[#] &;
  	labels = b[[All,1]];
  	c = Table[
    		If[Count[b[[i, 2]], {#, _}] > 0, 
       		b[[i, 2]][[Position[b[[i, 2]], #][[1]][[1]], 2]], 0] & /@ labels
    	,{i, 1, labels // Length}];
  	{c, labels}
]
GetExperimentHousesContactsTransitionsWithPattern[contactsSection_, pattern_] := Module[{a, persons, houses, partial, matrix, transitions, b, c, labels},
  	a = GetHousesContactTally[contactsSection];
  	b = a // Sort;
  	persons = b[[All, 1]] // Flatten;
  	houses = Flatten[a[[All, 2]], 1][[All, 1]] // DeleteDuplicates // Sort;
  	labels = {persons, houses} // Flatten;
  	partial = Table[If[Count[b[[i, 2]], {#, _}] > 0, b[[i, 2]][[Position[b[[i, 2]], #][[1]][[1]], 2]], 0] & /@ labels, {i, 1, b // Length}];
  	matrix = {partial, ConstantArray[ConstantArray[0, partial[[1]] // Length], houses // Length]} // Flatten[#, 1] &;
  	transitions = {matrix, labels}
]
GetExperimentSetContactsTransitionsWithPattern[contactsSections_,pattern_] := Module[{contacts},
	contacts = GetExperimentContactsTransitionsWithPattern[#,pattern]&/@contactsSections;
	{contacts[[All,1]]//Median,contacts[[1,2]]}	
]
GetExperimentSetHousesContactsTransitionsWithPattern[contactsSection_,pattern_] := Module[{transitions, means},
  	transitions = GetExperimentHousesContactsTransitionsWithPattern[#, {}] & /@ contactsSection;
 	means = (transitions[[All, 1]]) // Mean;
  	{means, transitions[[1, 2]]}
]
GetExperimentSetContactsTransitionsWithPatternRaw[contactsSections_, pattern_] := Module[{contacts}, 
  	contacts = GetExperimentContactsTransitionsWithPattern[#, pattern] & /@ contactsSections;
  	{contacts[[All, 1]], contacts[[1, 2]]}
]
GetExperimentSetHousesContactsTransitionsWithPatternRaw[contactsSections_, pattern_] := Module[{contacts}, 
  	contacts = GetExperimentHousesContactsTransitionsWithPattern[#, pattern] & /@ contactsSections;
  	{contacts[[All, 1]], contacts[[1, 2]]}
]

(*Experiments*)
LatinHypercubes[variablesRanges_, divisions_] := Module[{variablesRatios, diffs, delta, intervals, partitions, coordinates, randomCoordinates},
  	variablesRatios = ConstantArray[divisions, variablesRanges // Length];
  	diffs = (#[[2]] - #[[1]]) & /@ variablesRanges;
  	delta = (diffs/variablesRatios);
  	intervals = Range[#[[1, 1]], #[[1, 2]], #[[2]]] & /@ ({variablesRanges, delta} // Transpose);
  	partitions = Partition[#, 2, 1] & /@ intervals;
  	coordinates = (RandomSample /@ partitions) // Transpose;
  	randomCoordinates = Map[RandomReal, coordinates, {2}]
]

(*ImageProcessing*)
FindHouses[map_, morphoThreshold_, minSize_, maxSize_, rectangularity_] := Module[{morpho, img1, m, img2, common, replaced, m2, multi, coords, final},
  	morpho = MorphologicalComponents[map, morphoThreshold, CornerNeighbors -> True] // Colorize;
  	img1 = ImageCompose[map, {morpho, .5}];
  	m = SelectComponents[morpho, {"BoundingBoxArea", "BoundingBoxArea", "Rectangularity"}, #1 > minSize && #2 < maxSize && #3 > rectangularity &, -1] // Colorize;
  	img2 = ImageCompose[map, {m, .75}];common = Commonest[Flatten[ImageData[m], 1]][[1]];
  	replaced = Map[If[# == common, {0, 0, 0}, #] &, m // ImageData, {2}];
  	m2 = replaced // Image;
  	multi = ImageMultiply[map, m2];
  	coords = ComponentMeasurements[multi, {"Centroid", "BoundingBoxArea"}];
  	final = Show[map, Graphics[{Red, Thick, Circle[#[[1]], #[[2]]] & /@ ComponentMeasurements[multi, {"Centroid", "EquivalentDiskRadius"}][[All, 2]]}]];
  	{ImageResize[#, 750] & /@ {img1, img2, multi, final}, coords}
]
  
(*---------------------------------------*)
(*Initial Experiment Export Strict Format*)
GetRunHeader[rawRunData_] := rawRunData[[1 ;; 20]]
GetRunOutput[rawRunData_] := rawRunData[[21 ;; All]]
GetRunOutputWithTail[rawRunData_] := rawRunData[[21 ;; (Position[rawRunData, "--.--"] // Flatten)[[1]] - 1]]
GetRunTail[rawRunData_] := rawRunData[[(Position[rawRunData, "--.--"] // Flatten)[[1]] + 1 ;; All]]
GetExperimentHeaders[rawExperimentData_] := GetRunHeader[#] & /@ rawExperimentData
GetExperimentOutputs[rawExperimentData_] := GetRunOutput[#] & /@ rawExperimentData
GetExperimentOutputsWithTail[rawExperimentData_] := GetRunOutputWithTail[#] & /@ rawExperimentData
GetExperimentTails[rawExperimentData_] := GetRunTail[#] & /@ rawExperimentData

IndependentVariableBins[experimentHeaders_] := experimentHeaders[[All, 10]][[All, 2]] // DeleteDuplicates // Sort
IndependentVariableValues[experimentHeaders_] := experimentHeaders[[All, 10]][[All, 2]]
GetExperimentOutputsByBins[experimentHeaders_, experimentOutputs_] := Module[{indepBins, indepVals, tempPositions, tempArray},
  	indepBins = IndependentVariableBins[experimentHeaders];
  	indepVals = IndependentVariableValues[experimentHeaders];
  
  	tempArray = ConstantArray[{}, IndependentVariableBins[experimentHeaders] // Length];
  	tempPositions = Position[indepBins, #][[1]][[1]] & /@ indepVals;
  	Table[tempArray = ReplacePart[tempArray, tempPositions[[i]] -> Append[tempArray[[tempPositions[[i]]]], experimentOutputs[[i]]]],{i, 1, experimentOutputs // Length}];
  	tempArray
]
GetSelectedDependentVariableVertical[binOutput_, desiredOutputIndex_] := (Map[Transpose, #][[All,desiredOutputIndex]] // Transpose) & /@ binOutput
GetSelectedDependentVariableHorizontal[binOutput_, desiredOutputIndex_] := (Map[Transpose, #][[All,desiredOutputIndex]]) & /@ binOutput
GetSelectedDependentVariableOnTick[binOutput_, desiredOutputIndex_, tickNumber_] := GetSelectedDependentVariableVertical[binOutput,desiredOutputIndex][[All, tickNumber]]

(*Auxiliary Functions*)
NetLogoListToMathematicaList[data_] := StringReplace[data, {" " -> ",", "[" -> "{", "]" -> "}"}] // ToExpression // Flatten
FixBitLists[experimentTails_] := (Map[NetLogoListToMathematicaList, experimentTails, {2}])
SensitivityMeasures[samplesList_]:=Module[{ticks,min,mean,median,max,sd},
	ticks = samplesList // Transpose;
	min = Min /@ ticks;
	mean = Mean /@ ticks;
	median = Median /@ ticks;
	max = Max /@ ticks;
	sd = StandardDeviation /@ ticks;
	{min,mean,median,max,sd}
]
FilterVariablesWithRanges[data_, probingPattern_, ranges_] := Module[{logic, in, out, variables, variables2, restrictions, restrictionPattern},
  	(*Differentiate inputs from outputs*)
  	logic = NumberQ /@ probingPattern;
  	(*Generate variables patterns {i1,i2,...,in,o1,o2,...,on}*)
  	in = 0; out = 0;
  	variables = Table[If[logic[[i]],in++; ("i" <> ToString[in] <> "_") // ToExpression,out++; ("o" <> ToString[out] <> "_") // ToExpression], {i, 1, logic // Length}];
  	in = 0; out = 0;
  	variables2 = Table[If[logic[[i]],in++; ("i" <> ToString[in]) // ToExpression,out++; ("o" <> ToString[out]) // ToExpression], {i, 1, logic // Length}];
  	(*Create restrictions patterns*)
  	restrictions = (probingPattern[[#]] - ranges[[#]] < variables2[[#]] < probingPattern[[#]] + ranges[[#]]) & /@ Range[Count[logic, True]];
  	restrictionPattern = StringReplace[restrictions // ToString, {"{" -> "", "}" -> "", "," -> " &&"}] //ToExpression;
  	(*Return filtered data*)
  	Cases[data, variables /; (restrictionPattern // Evaluate)]
]

(*Social Network Analysis*)
BitGraph[subsets_, numberOfIntersections_] := Module[{graphConnections, graphEdgesThickness, graphEdgesTooltip, max, min},
  	graphConnections = Apply[UndirectedEdge, #] & /@ subsets;
  	max = numberOfIntersections // Max;
 	min = numberOfIntersections // Min;
  	graphEdgesThickness = Rule[Apply[UndirectedEdge, #[[1]]], Thickness[#[[2]]/(max*25)]] & /@ ({subsets, numberOfIntersections} // Transpose);
  	graphEdgesTooltip = Tooltip[Rule[Apply[UndirectedEdge, #[[1]]], #[[2]]/100]] & /@ ({subsets, numberOfIntersections} // Transpose);
  
  	Graph[graphConnections,
   		EdgeStyle -> Append[graphEdgesThickness, graphEdgesTooltip],
   		VertexLabels -> Placed["Name", {1/2, 1/2}],
   		VertexLabelStyle -> Directive[FontFamily -> "Courier", 20, Black, Bold],
   		VertexSize -> Medium,
   		VertexStyle -> LightGreen
	]
]
BitIntersectionCount[bitList_] := Module[{length, range, subsets, intersections, numberOfIntersections},
  	length = bitList // Length;
  	range = Range[length];
  	subsets = Subsets[range, {2}];
  	intersections = Apply[Intersection, bitList[[#]]] & /@ subsets;
  	numberOfIntersections = Length /@ intersections;
  	({subsets, numberOfIntersections} // Transpose);
  	{subsets, numberOfIntersections}
]
ExperimentIntersectionCount[bitLists_] := BitIntersectionCount[#] & /@ bitLists
TotalExperimentIntersectionCount[bitLists_] := Module[{list},
	list = ExperimentIntersectionCount[bitLists];
  	{list[[1]][[1]], list[[All, 2]] // Total}
]

FixBitesListTail[experimentTail_] := (StringReplace[#, {"[" -> " ", "]" -> " ", " " -> ","}] // ToString // ToExpression) & /@ experimentTail
FixBitesListTails[experimentsTails_] := FixBitesListTail /@ experimentsTails
GetRunTailIDs[runTail_] := #[[All, 1]] & /@ runTail
GetRunTailTicks[runTail_] := #[[All, 2]] & /@ runTail
GetSubsetsIDsIntersectionsOfRun[subsetsIDs_] := Map[Apply[Intersection, #] &, subsetsIDs]
GetSubsetsIntersectionPositions[subsetsIDs_, subsetsIntersections_] := Module[{output},
	output=Table[
		If[
			(subsetsIntersections[[i]] // Length) > 0,
			({Position[subsetsIDs[[i]][[1]], #] // Flatten, Position[subsetsIDs[[i]][[2]], #] // Flatten} & /@ subsetsIntersections[[i]]) // Transpose,
			{{0},{0}}
		]
	,{i, 1, subsetsIntersections // Length}];
	output
]
CountRunBloodExchanges[ticksIntersected_] := Table[CountBloodExchanges[ticksIntersected[[i]][[1]], ticksIntersected[[i]][[2]]], {i, 1, ticksIntersected // Length}]
GetIntersectedTicks[subsets_, intersectionsPositions_, bitesTicks_] := Module[{},
	Table[
  			{
   				bitesTicks[[subsets[[i]][[1]]]][[#]] & /@ intersectionsPositions[[i]][[1]],
   				bitesTicks[[subsets[[i]][[2]]]][[#]] & /@ intersectionsPositions[[i]][[2]]
   			}
  	,{i, 1, subsets // Length}]
]
GetSubsetsIDsAndTicksOfRun[bitesList_] := Module[{subsets, runIDs, runTicks, subsetsIDs, subsetsTicks},
  	subsets = Subsets[Range[bitesList // Length], {2}];
  	runIDs = GetRunTailIDs[bitesList];
  	runTicks = GetRunTailTicks[bitesList];
  
  	subsetsIDs = runIDs[[#]] & /@ subsets;
  	subsetsTicks = runTicks[[#]] & /@ subsets;
  
  	{subsets, subsetsIDs, subsetsTicks}
]
CountBloodExchanges[indA_, indB_] := Module[{a, b, ordering, changesList, pair, AToB, BToA, output, preChangesList},
  	(*Counts how many times individual A has been infected by B and viceversa according to ticks lists*)
  	(*Contains potential error yielded when an individual does not infect any other!!!!!!!!!!!!!!!!!!!!!!!*)
  	output = Table[
     		a = indA[[j]];
     		b = indB[[j]];
     		ordering = {a, b} // Flatten // Ordering;
     		preChangesList = ({ConstantArray[1, a // Length], ConstantArray[2, b // Length]} // Flatten);
     		If[(preChangesList//Length) > 0 ,
     			changesList = preChangesList[[ordering]];
     			Table[
      				pair = RotateLeft[changesList, i][[1 ;; 2]];
      				If[pair[[1]] != pair[[2]], If[pair[[1]] > pair[[2]], "b-a", "a-b"]]
      			,{i, 0, (changesList // Length) - 2}]
      		,##&[]
     		]
   			,{j, 1, indA // Length}] // Flatten;
  	AToB = Cases[output, "b-a"] // Length;
  	BToA = Cases[output, "a-b"] // Length;
  	{BToA, AToB}
]
GetRunBloodExchanges[bitesList_] := Module[{output, subsets, subsetsIntersections, intersectionsPositions, ticksIntersected, bloodExchanges, subsetsIDs, subsetsTicks, bitesTicks},
  	output = GetSubsetsIDsAndTicksOfRun[bitesList];
  	bitesTicks = GetRunTailTicks[bitesList];
  	subsets = output[[1]]; subsetsIDs = output[[2]]; subsetsTicks = output[[3]];
  	subsetsIntersections = GetSubsetsIDsIntersectionsOfRun[subsetsIDs];
  	intersectionsPositions = GetSubsetsIntersectionPositions[subsetsIDs, subsetsIntersections];
  	ticksIntersected = GetIntersectedTicks[subsets, intersectionsPositions, bitesTicks];
  	bloodExchanges = CountRunBloodExchanges[ticksIntersected];
  	{subsets, bloodExchanges}
]
GetExperimentBloodExchanges[bitesLists_]:= GetRunBloodExchanges[#] & /@ bitesLists
InfectionMatrixRun[bloodExchanges_] := Module[{header, infections, matrix},
  	header = bloodExchanges[[1]];
  	infections = bloodExchanges[[2]];
  	matrix = Table[ConstantArray[0, header//Flatten//DeleteDuplicates//Length], {i, header//Flatten//DeleteDuplicates//Length}];
  	Table[
   		matrix = ReplacePart[matrix, header[[i]] -> infections[[i]][[1]]];
   		matrix = ReplacePart[matrix, Reverse[header[[i]]] -> infections[[i]][[2]]];
   	,{i, 1, header // Length}];
  	{header, matrix}
]
InfectionMatrixExperiment[experimentBloodExchanges_] := Module[{data, header, matrix},
  	data = InfectionMatrixRun /@ experimentBloodExchanges;
  	header = data[[1]][[1]];
  	matrix = data[[All, 2]] // Total;
  	{header, matrix}
]
MarkovFromFrequencyMatrix[frequencyMatrix_] := Table[If[(frequencyMatrix[[i]] // Total) != 0, frequencyMatrix[[i]]/(frequencyMatrix[[i]] // Total), ReplacePart[ConstantArray[0, Length[frequencyMatrix]], i -> 1]],{i, 1, frequencyMatrix // Length}]

CreateEdges[process_, syllables_] := With[{sm = MarkovProcessProperties[process, "TransitionMatrix"]}, Flatten@Table[DirectedEdge[i, j] -> sm[[i, j]], {i, Length[syllables]}, {j, Length[syllables]}]];
RemoveEdgesWithZeroTransitionProbability[rawEdges_] := Table[If[rawEdges[[i]][[2]] == 0, ## &[], rawEdges[[i]]], {i, 1, Length[rawEdges]}]
VertexNumberToVertexSyllable[syllables_, fixedEdgesList_] := Table[DirectedEdge[syllables[[fixedEdgesList[[index]][[1]][[1]]]], syllables[[fixedEdgesList[[index]][[1]][[2]]]] -> fixedEdgesList[[index]][[2]]] // N, {index, 1, Length[fixedEdgesList]}]
Options[EdgesStyleList] := {LineThickness -> .004, VariableStyle -> "Both"}
EdgesStyleList[vertexSyllableList_, OptionsPattern[]] := Switch[OptionValue[VariableStyle], "LineColor", Table[DirectedEdge[vertexSyllableList[[i]][[1]], vertexSyllableList[[i]][[2]][[1]]] -> {Hue[.85*vertexSyllableList[[i]][[2]][[2]] + .15], Thickness[OptionValue[LineThickness]]}, {i, 1, Length[vertexSyllableList]}], "LineWidth", Table[DirectedEdge[vertexSyllableList[[i]][[1]], vertexSyllableList[[i]][[2]][[1]]] -> {Thickness[vertexSyllableList[[i]][[2]][[2]]*OptionValue[LineThickness]]}, {i, 1, Length[vertexSyllableList]}], "Both", Table[DirectedEdge[vertexSyllableList[[i]][[1]], vertexSyllableList[[i]][[2]][[1]]] -> {Hue[.85*vertexSyllableList[[i]][[2]][[2]] + .15], Thickness[ vertexSyllableList[[i]][[2]][[2]]*OptionValue[LineThickness]]}, {i, 1, Length[vertexSyllableList]}], _, Table[DirectedEdge[vertexSyllableList[[i]][[1]], vertexSyllableList[[i]][[2]][[1]]] -> {Thickness[OptionValue[LineThickness]]}, {i, 1, Length[vertexSyllableList]}]]
Options[MarkovSyllablesStyledGraph] := {GraphLayout -> Automatic, ImageSize -> 1000, VertexSize -> .25, EdgeShapeFunction -> edgeshape, VertexLabelStyle -> Automatic}
edgeshape[e_, ___] := {Arrowheads[{{.05, .5}}], Arrow[e]}
MarkovSyllablesStyledGraph[syllables_, process_, edgesStyleList_, OptionsPattern[]] := Graph[syllables, process, 
	ImageSize -> OptionValue[ImageSize], 
  	EdgeStyle -> edgesStyleList, 
  	GraphLayout -> OptionValue[GraphLayout], 
  	EdgeShapeFunction -> OptionValue[EdgeShapeFunction], 
  	VertexSize -> OptionValue[VertexSize], 
  	VertexLabelStyle -> OptionValue[VertexLabelStyle]
]
Options[MarkovGraph] := {LineThickness -> .01, VariableStyle -> "Both", GraphLayout -> Automatic, ImageSize -> Automatic, VertexSize -> .5, EdgeShapeFunction -> edgeshape, VertexLabelStyle -> {Italic}}
MarkovGraph[markovOutput_, OptionsPattern[]] := Module[{rawEdgesList, fixedEdgesList, vertexSyllableList, edgesStyleList}, 
  	rawEdgesList = CreateEdges[DiscreteMarkovProcess[1, markovOutput[[1]]], 
    markovOutput[[2]]];
  	fixedEdgesList = RemoveEdgesWithZeroTransitionProbability[rawEdgesList];
  	vertexSyllableList = VertexNumberToVertexSyllable[markovOutput[[2]], fixedEdgesList];
  	edgesStyleList = EdgesStyleList[vertexSyllableList, LineThickness -> OptionValue[LineThickness], VariableStyle -> OptionValue[VariableStyle]];
	MarkovSyllablesStyledGraph[markovOutput[[2]], DiscreteMarkovProcess[1, markovOutput[[1]]], edgesStyleList, 
   		ImageSize -> OptionValue[ImageSize], 
   		GraphLayout -> OptionValue[GraphLayout], 
   		EdgeShapeFunction -> OptionValue[EdgeShapeFunction], 
   		VertexSize -> OptionValue[VertexSize], 
   		VertexLabelStyle -> OptionValue[VertexLabelStyle]
   	]
]

(*CA Functions*)
SeparateAutomaton[automata_] := Table[#[[All, i]] & /@ automata, {i, 1, automata[[1]][[1]] // Length}]
EvaluateCellularAutomaton[automata_, neighborhoodType_, functions_, cellFunction_] := Module[{vonNewmanNeighborhood, mooreNeighborhood, automataSeparated, automataNeighbours, automataNeighboursReleased, appliedSeparateAutomata, mergedMatrix},
  	(*Define the type of neighbourhood to use*)
  	vonNewmanNeighborhood = {{{a_, b_, c_}, {d_, e_, f_}, {g_, h_, i_}} -> Hold[{b, d, e, f, h}]};
  	mooreNeighborhood = {{{a_, b_, c_}, {d_, e_, f_}, {g_, h_, i_}} -> Hold[{a, b, c, d, e, f, g, h, i}]};
  	(*Separate different automata*)
	automataSeparated = SeparateAutomaton[automata];
  	(*Obtain the neighbourhoods of the automata*)
  	automataNeighbours = CellularAutomaton[vonNewmanNeighborhood, #, 1] & /@ automataSeparated;
  	automataNeighboursReleased = (Map[ReleaseHold, automataNeighbours[[#]]] // Last) & /@ Range[automataNeighbours // Length];
  	(*Evaluate Selected Functions for each Separate Automata*)
  	appliedSeparateAutomata = Map[functions[[#]], automataNeighboursReleased[[#]], {2}] & /@ Range[automataNeighboursReleased // Length];
  	(*Merge the Separate Cellular Automata*)
  	mergedMatrix = MergeAutomata[appliedSeparateAutomata];
  	{Map[Apply[cellFunction, #] &, mergedMatrix, {2}],neighborhoodType,functions,cellFunction}
]
MergeAutomata[automata_]:=(automata[[All, #]] // Transpose) & /@Range[automata[[1]] // Length];
NestListEvaluateCellularAutomaton[automata_,neighborhoodType_,functions_,cellFunction_,epochs_]:=NestList[(Apply[EvaluateCellularAutomaton[#1, #2, #3, #4] &, #]) &, {automata,neighborhoodType,functions,cellFunction}, epochs]
NestEvaluateCellularAutomaton[automata_,neighborhoodType_,functions_,cellFunction_,epochs_]:=Nest[(Apply[EvaluateCellularAutomaton[#1, #2, #3, #4] &, #]) &, {automata,neighborhoodType,functions,cellFunction}, epochs]
ExtractKernelValuesAndPositions[automaton_, kernelCenter_, kernel_] :=Module[{tempR, tempC, kLength, sLength, rRange, cRange, rValues, cValues, listOfPossibleElements, submatrix, selectedWithKernel, kernelValues, kernelPositions},
  	tempR = kernelCenter[[1]];
  	tempC = kernelCenter[[2]];
  	kLength = kernel // Length; sLength = (kLength - 1)/2;
  	(*Ranges of values to extract from matrix*) 
  	rRange = {tempR - sLength, tempR + sLength};
  	cRange = {tempC - sLength, tempC + sLength};
  	(*Specific positions to extract from matrix*)  
  	rValues = Apply[Range, rRange];
 	cValues = Apply[Range, cRange];
  	listOfPossibleElements = Partition[Tuples[{rValues, cValues}], kLength];
  	(*Values of positions of matrix*)
  	submatrix = automaton[[rRange[[1]] ;; rRange[[2]], cRange[[1]] ;; cRange[[2]]]];
  	(*One to one reference of elements and positions*)
  	selectedWithKernel = Position[kernel, 1];
  	kernelValues = Map[Apply[submatrix[[##]] &, #] &, selectedWithKernel];
  	kernelPositions = Map[Apply[listOfPossibleElements[[##]] &, #] &, selectedWithKernel];
  	{kernelValues, kernelPositions}
]
ReplaceKernelValuesWithPositions[automaton_, kernelValuesAndPositions_] := Module[{replaceRules},
  	replaceRules = #[[2]] -> #[[1]] & /@ (kernelValuesAndPositions // Transpose);
  	replaceRules = DeleteCases[replaceRules, {0, _} -> _];
  	replaceRules = DeleteCases[replaceRules, {_, 0} -> _];
  	ReplacePart[automaton, replaceRules]
]
GenerateProcessedCellsMatrix[automaton_] := ConstantArray[False, {automaton // Length, automaton[[1]] // Length}]
UpdateProcessedMatrix[processedMatrix_, index_] := ReplacePart[processedMatrix, index -> True]
RandomUnprocessedPosition[processedMatrix_] := (Position[processedMatrix, False] // RandomSample)[[1]]
FixSize[automaton_, kernel_, paddingValue_] := ArrayPad[automaton,((kernel // Length) - 1)/2, paddingValue]
(*Under Review*)
EvaluateRandomCell[automaton_, processedMatrix_, kernel_, kernelFunction_, matrixFixValue_] := Module[{valuesAndPositions, evaluatedKernel, updateProcessedMatrix, updatedAutomata, fixedSizeMatrix, sKernel, randomCell},
  	fixedSizeMatrix = FixSize[automaton, kernel, matrixFixValue];
  	sKernel = ((kernel // Length) - 1)/2;
  	randomCell = RandomUnprocessedPosition[processedMatrix] + sKernel;
  	valuesAndPositions = ExtractKernelValuesAndPositions[fixedSizeMatrix, randomCell, kernel];
  	(*Apply Function*)
	evaluatedKernel = {Apply[kernelFunction, {valuesAndPositions[[1]]}],valuesAndPositions[[2]] - sKernel};
  	(*Apply Function*)
	updateProcessedMatrix = UpdateProcessedMatrix[processedMatrix, randomCell - sKernel];
	updatedAutomata = ReplaceKernelValuesWithPositions[automaton, evaluatedKernel];
	(*Print[{randomCell-sKernel,updatedAutomata//MatrixForm}];*)
  	{updatedAutomata, updateProcessedMatrix, kernel, kernelFunction, matrixFixValue}
]
EvaluateOneEpoch[automaton_, kernel_, kernelFunction_, matrixFixValue_] := Module[{processedMatrix,output},
  	processedMatrix = GenerateProcessedCellsMatrix[automaton];
  	output = Nest[(Apply[EvaluateRandomCell[#1, #2, #3, #4, #5] &, #]) &, {automaton, processedMatrix, kernel, kernelFunction, matrixFixValue}, (automaton // Length)*(automaton[[1]] // Length)];
	Delete[output,2]
]
EvaluateCAThroughEpochs[initialAutomaton_, kernel_, kernelFunction_, padValue_, epochsNumber_, historicBool_] :=
 	If[historicBool,
 		NestList[(Apply[EvaluateOneEpoch[#1, #2, #3, #4] &, #]) &, {initialAutomaton, kernel, kernelFunction, padValue}, epochsNumber][[All, 1]],
  		Nest[(Apply[EvaluateOneEpoch[#1, #2, #3, #4] &, #]) &, {initialAutomaton, kernel, kernelFunction, padValue}, epochsNumber][[1]]
]

(*Legacy*)
GetPositionsByIndVarBins[experimentsXMLData_] := Module[{parametersList, indParPos, indParVal, tuples, patterns, constant, indParList, tuplesPositions},
  	parametersList = GetExperimentParameters[#] & /@ experimentsXMLData;
  	indParPos = GetExperimentsSetParameters[experimentsXMLData] // Position[#, {"independent", _, _}] & // Flatten;
  	indParVal = (GetExperimentsSetParameters[experimentsXMLData] // Cases[#, {"independent", _, a_} -> a] &);
  	tuples = Tuples[indParVal];
	patterns = Table[
			constant = ConstantArray[{_, _, "dummy"}, parametersList[[1]][[indParPos]] // Length];
    		ReplacePart[constant[[j]], 3 -> tuples[[i]][[j]]]
    	,{i, 1, tuples // Length}, {j, 1, tuples[[1]] // Length}];
  	indParList = #[[indParPos]] & /@ parametersList; 
    tuplesPositions = Position[ToExpression /@ indParList, #] & /@ patterns;
  	{tuplesPositions, tuples} // Transpose
]
GetOutputFromExperimentPositions[experimentsXMLData_, positionsList_, outputTag_] := Module[{experimentsOutputsSections, output},
  	experimentsOutputsSections = GetExperimentSection[#, "output"] & /@ experimentsXMLData[[positionsList]];
  	output = Cases[#, XMLElement["output", {"id" -> outputTag}, {a_}] -> a] & /@experimentsOutputsSections;
  	Map[ToExpression, output, Infinity]
]


(*NewFunctions*)
ConvertTicksToIntegers[cSect_] := Module[{newCSect, head, tail},
  newCSect = {Table[
     head = cSect[[1]][[i, 1]];
     tail = cSect[[1]][[i, 2 ;; All]];
     {tail[[All, 1]], tail[[All, 2]] // ToExpression} // Transpose // Prepend[#, head] &
     , {i, 1, cSect[[1]] // Length}]}
]
FilterBetweenTicks[fixedTicksCSect_, low_, high_] := Module[{filteredCSect, section, newCSect = fixedTicksCSect},
  filteredCSect = {Table[
     section = newCSect[[1]][[i]];
     Cases[section[[2 ;; All]], {a_, b_} /; (b >= low && b <= high)] //
       Prepend[#, section[[1]]] &
     , {i, 1, newCSect[[1]] // Length}]}
]
GetExperimentSetContactsTransitionsWithPatternInIntervals[fixedTicksCSect_, frames_] := Module[{max, increment, low, high, fCSect,intervals},
  	max = fixedTicksCSect[[1]][[All, 2 ;; All]][[All, All, 2]] // Flatten // Max;
  	increment = max/frames;
  	intervals = Table[
    	low = 0; high = i;
    	fCSect = FilterBetweenTicks[fixedTicksCSect, low, high];
    	GetExperimentSetContactsTransitionsWithPattern[fCSect, {}]
    ,{i, 0, max, increment}]
]
Options[GraphTransitionsFrequenciesWithMax] := {LineThickness -> .0075, VariableStyle -> "Both", GraphLayout -> Automatic, ImageSize -> Automatic, VertexSize -> .5, EdgeShapeFunction -> edgeshape, VertexLabelStyle -> {Italic}}
GraphTransitionsFrequenciesWithMax[frequenciesOutput_, max_, OptionsPattern[]] := Module[{frequencies, syllables, edges, fixedEdgesList, vertexSyllableList, normalizedVertexSyllableList,edgesStyleList, links},
  	frequencies = frequenciesOutput[[1]];
  	syllables = frequenciesOutput[[2]];
  	edges = Table[DirectedEdge[i, j] -> frequencies[[i, j]], {i, Length[syllables]}, {j, Length[syllables]}] // Flatten;
  	fixedEdgesList = DeleteCases[edges, DirectedEdge[_, _] -> 0];
  	vertexSyllableList = VertexNumberToVertexSyllable[frequenciesOutput[[2]], fixedEdgesList];
	normalizedVertexSyllableList = (DirectedEdge[#[[1]], #[[2]][[1]]] -> #[[2]][[2]]/max &) /@ vertexSyllableList;
  	edgesStyleList = EdgesStyleList2[normalizedVertexSyllableList, LineThickness -> OptionValue[LineThickness], VariableStyle -> OptionValue[VariableStyle]];
  	links = DirectedEdge[#[[1]], #[[2, 1]]] & /@ vertexSyllableList;
  	Graph[syllables, links, EdgeStyle -> edgesStyleList, ImageSize -> OptionValue[ImageSize], GraphLayout -> OptionValue[GraphLayout], EdgeShapeFunction -> OptionValue[EdgeShapeFunction], VertexSize -> OptionValue[VertexSize], VertexLabelStyle -> OptionValue[VertexLabelStyle], VertexLabels -> Placed["Name", Center],EdgeLabels -> Table[links[[i]] -> Placed[fixedEdgesList[[All, 2]][[i]], Tooltip], {i, Length[links]}]]
]
Options[GraphTransitionsFrequenciesSet] := {LineThickness -> .0075, VariableStyle -> "Both", GraphLayout -> Automatic, ImageSize -> Automatic, VertexSize -> .5, EdgeShapeFunction -> edgeshape, VertexLabelStyle -> {Italic}}
GraphTransitionsFrequenciesSet[matricesSet_, OptionsPattern[]] := GraphTransitionsFrequenciesWithMax[#, matricesSet[[All, 1]] // Flatten // Max, 
    LineThickness -> OptionValue[LineThickness], 
    VariableStyle -> OptionValue[VariableStyle], 
    GraphLayout -> OptionValue[GraphLayout], 
    ImageSize -> OptionValue[ImageSize], 
    VertexSize -> OptionValue[VertexSize], 
    EdgeShapeFunction -> OptionValue[EdgeShapeFunction], 
    VertexLabelStyle -> OptionValue[VertexLabelStyle]] & /@ matricesSet
Options[EdgesStyleList2] := {LineThickness -> .004, VariableStyle -> "Both"}
EdgesStyleList2[vertexSyllableList_, OptionsPattern[]] := Switch[OptionValue[VariableStyle],
  		"LineColor", Table[DirectedEdge[vertexSyllableList[[i]][[1]][[1]], vertexSyllableList[[i]][[1]][[2]]] -> {Hue[.85*vertexSyllableList[[i]][[2]] + .15], Thickness[OptionValue[LineThickness]]}, {i, 1, Length[vertexSyllableList]}],
  		"LineWidth", Table[DirectedEdge[vertexSyllableList[[i]][[1]][[1]], vertexSyllableList[[i]][[1]][[2]]] -> {Thickness[vertexSyllableList[[i]][[2]]*OptionValue[LineThickness]]}, {i, 1, Length[vertexSyllableList]}],
  		"Both", Table[DirectedEdge[vertexSyllableList[[i]][[1]][[1]], vertexSyllableList[[i]][[1]][[2]]] -> {Hue[.85*vertexSyllableList[[i]][[2]] + .15], Thickness[vertexSyllableList[[i]][[2]]*OptionValue[LineThickness]]},{i, 1, Length[vertexSyllableList]}], 
  		_, Table[DirectedEdge[vertexSyllableList[[i]][[1]][[1]], vertexSyllableList[[i]][[1]][[2]]] -> {Thickness[OptionValue[LineThickness]]}, {i, 1, Length[vertexSyllableList]}]
  	]
VertexNumberToVertexSyllable[syllables_, fixedEdgesList_] := Table[ DirectedEdge[syllables[[fixedEdgesList[[index]][[1]][[1]]]], syllables[[fixedEdgesList[[index]][[1]][[2]]]] -> fixedEdgesList[[index]][[2]]] // N, {index, 1, Length[fixedEdgesList]}]


End[]
EndPackage[]

