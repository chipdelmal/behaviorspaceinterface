# Bahaviour Space Interface 
Companion package to SoNA3BS. Please refer to the following address for more information: https://github.com/Chipdelmal/SoNA3BS


## Contact

* mail: sanchez.hmsc@itesm.com
* website: https://sites.google.com/site/sona3bs/ 

## Licence

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">SoNA3BS</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://sites.google.com/site/sona3bs/" property="cc:attributionName" rel="cc:attributionURL">Héctor Manuel Sánchez Castellanos</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
